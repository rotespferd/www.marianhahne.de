module.exports = function (eleventyConfig) {
    eleventyConfig.setTemplateFormats([
        "md",
        "css",
        "html",
        "eot",
        "svg",
        "woff",
        "woff2",
        "ttf"
    ]);

    eleventyConfig.addPassthroughCopy("img");
};